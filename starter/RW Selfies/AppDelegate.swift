/**
 * Copyright (c) 2017 Razeware LLC
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

import UIKit
import StoreKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
  
  var window: UIWindow?
  
  func application(_ application: UIApplication,
                   didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
    SKPaymentQueue.default().add(self)
    SubscriptionService.shared.loadSubscriptionOptions()
    
    
    // verify is there is a valid subscription
    SubscriptionService.shared.uploadReceipt { (success) in
      DispatchQueue.main.async {
        NotificationCenter.default.post(name: SubscriptionService.purchaseSuccessfulNotification, object: nil)
      }
    }
    
    return true
  }
}

extension AppDelegate: SKPaymentTransactionObserver {
  
  func paymentQueue(_ queue: SKPaymentQueue,
                    updatedTransactions transactions: [SKPaymentTransaction]) {
    for transaction in transactions {
      switch transaction.transactionState {
      case .purchasing:
        handlePurchasingState(for: transaction, in: queue)
      case .purchased:
        handlePurchasedState(for: transaction, in: queue)
      case .restored:
        handleRestoredState(for: transaction, in: queue)
      case .failed:
        handleFailedState(for: transaction, in: queue)
      case .deferred:
        handleDeferredState(for: transaction, in: queue)
      }
    }
  }
  
  func handlePurchasingState(for transaction: SKPaymentTransaction, in queue: SKPaymentQueue) {
    print("User is attempting to purchase product id: \(transaction.payment.productIdentifier)")
  }
  
  func handlePurchasedState(for transaction: SKPaymentTransaction, in queue: SKPaymentQueue) {
    print("User purchased product id: \(transaction.payment.productIdentifier)")
    
    queue.finishTransaction(transaction)
    SubscriptionService.shared.uploadReceipt { (success) in
      DispatchQueue.main.async {
        NotificationCenter.default.post(name: SubscriptionService.purchaseSuccessfulNotification, object: nil)
      }
    }
  }
  
  func handleRestoredState(for transaction: SKPaymentTransaction, in queue: SKPaymentQueue) {
    print("Purchase restored for product id: \(transaction.payment.productIdentifier)")
    queue.finishTransaction(transaction)
    SubscriptionService.shared.uploadReceipt { (success) in
      DispatchQueue.main.async {
        NotificationCenter.default.post(name: SubscriptionService.restoreSuccessfulNotification, object: nil)
      }
    }
  }
  
  func handleFailedState(for transaction: SKPaymentTransaction, in queue: SKPaymentQueue) {
    print("Purchase failed for product id: \(transaction.payment.productIdentifier)")
    if ((transaction.error) != nil) {
      print("error: \(transaction.error?.localizedDescription)");
    }
    queue.finishTransaction(transaction);
  }
  
  func handleDeferredState(for transaction: SKPaymentTransaction, in queue: SKPaymentQueue) {
    print("Purchase deferred for product id: \(transaction.payment.productIdentifier)")
  }
  
}
