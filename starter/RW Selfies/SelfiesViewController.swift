/**
 * Copyright (c) 2017 Razeware LLC
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

import UIKit
import SelfieService

class SelfiesViewController: UIViewController {
  
  // MARK: - Outlets
  
  @IBOutlet var weekLabel: UILabel!
  @IBOutlet var collectionView: UICollectionView!
  
  // MARK: - Instance Properties
  
  override var preferredStatusBarStyle: UIStatusBarStyle { return .lightContent }
  private var restoreInProgressAlert: UIAlertController?
  fileprivate var selfieSets: [SelfieSet] = []
  
  
  // MARK: - Object Lifecycle
  
  deinit {
    NotificationCenter.default.removeObserver(self)
  }
  
  // MARK: - View Lifecycle
  
  override func viewDidLoad() {
    super.viewDidLoad()
    guard SubscriptionService.shared.currentSessionId != nil,
      SubscriptionService.shared.hasReceiptData,
      SubscriptionService.shared.currentSubscription != nil else {
        showRestoreAlert()
        return
    }
    loadSelfies()
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    navigationController?.setNavigationBarHidden(false, animated: true)
  }
  
  override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
    super.viewWillTransition(to: size, with: coordinator)
    collectionView.collectionViewLayout.invalidateLayout()
  }
  
  private func loadSelfies() {
    
    let sessionId = SubscriptionService.shared.currentSessionId ?? ""
    
    SelfieService.shared.selfies(for: sessionId) { [weak self] result in
      guard let strongSelf = self else { return }
      switch result {
      case .success(let selfies):
        strongSelf.selfieSets = selfies
        strongSelf.collectionView.reloadData()
        strongSelf.collectionView.setContentOffset(.zero, animated: true)
      case .failure(let error): strongSelf.showErrorAlert(for: error)
      }
      
    }
  }
  
  private func showErrorAlert(for error: SelfieServiceError) {
    let title: String
    let message: String
    switch error {
    case .missingAccountSecret:
      title = "Account Secret Not Configured"
      message = "You missed a step and did not add your iTunes Connect Account Secret, search in the tutorial for \"YOUR_ACCOUNT_SECRET\" for more info."
    case .invalidSession:
      title = "Invalid Session"
      message = "Please go back and try again or relaunch the app."
    case .noActiveSubscription:
      title = "No Active Subscription"
      message = "Please verify that you have an active subscription or sign up for a new one."
    case .other(let otherError):
      title = "Unexpected Error"
      message = otherError.localizedDescription
    }
    
    let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
    let backAction = UIAlertAction(title: "Back", style: .default) { (_) in
      _ = self.navigationController?.popViewController(animated: true)
    }
    
    alert.addAction(backAction)
    present(alert, animated: true, completion: nil)
  }
  
  private func showRestoreAlert() {
    let alert = UIAlertController(title: "Subscription Issue", message: "We are having a hard time finding your subscription. If you've recently reinstalled the app or got a new device please choose to restore your purchase. Otherwise go Back to Subscribe.", preferredStyle: .alert)
    
    let restoreAction = UIAlertAction(title: "Restore", style: .default) { [weak self] _ in
      SubscriptionService.shared.restorePurchases()
      self?.showRestoreInProgressAlert()
    }
    
    let backAction = UIAlertAction(title: "Back", style: .cancel) { _ in
      _ = self.navigationController?.popViewController(animated: true)
    }
    
    alert.addAction(restoreAction)
    alert.addAction(backAction)
    
    present(alert, animated: true, completion: nil)
  }
  
  private func showRestoreInProgressAlert() {
    let alert = UIAlertController(title: "Restoring Purchase", message: "Your purchase history is being restored. Upon completion this dialog will close and you will be sent back to the previous screen where you can then comeback in to load your purchases.", preferredStyle: .alert)
    present(alert, animated: true, completion: nil)
    
    NotificationCenter.default.addObserver(self, selector: #selector(SelfiesViewController.dismissRestoreInProgressAlert(notification:)), name: SubscriptionService.restoreSuccessfulNotification, object: nil)
  }
  
  @objc private func dismissRestoreInProgressAlert(notification: Notification) {
    dismiss(animated: true, completion: nil)
    _ = navigationController?.popViewController(animated: true)
  }  
}

// MARK: - UICollectionViewDataSource

extension SelfiesViewController: UICollectionViewDataSource {
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return selfieSets[section].selfies.count
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Selfie", for: indexPath) as! SelfieCollectionViewCell
    let selfieSet = selfieSets[indexPath.section]
    cell.configure(with: selfieSet.selfies[indexPath.row])
    
    return cell
  }
  
  func numberOfSections(in collectionView: UICollectionView) -> Int {
    return selfieSets.count
  }
  
  func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
    guard kind == UICollectionElementKindSectionHeader else {
      // no footers
      return UICollectionReusableView(frame: .zero)
    }
    
    let sectionView = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "Section", for: indexPath) as! SectionCollectionReusableView
    let selfieSet = selfieSets[indexPath.section]
    sectionView.nameLabel.text = selfieSet.name
    
    return sectionView
  }
}

// MARK: - UICollectionViewDelegateFlowLayout

extension SelfiesViewController: UICollectionViewDelegateFlowLayout {
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    let selfie = selfieSets[indexPath.section].selfies[indexPath.row]
    let imageSize = selfie.image.size
    let scale = view.frame.width / imageSize.width
    
    return CGSize(width: view.frame.width, height: imageSize.height * scale)
  }
}

