/**
 * Copyright (c) 2017 Razeware LLC
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

import UIKit
import StoreKit

class WelcomeViewController: UIViewController {
  
  // MARK: - Outlets
  
  @IBOutlet var enterButton: UIButton!
  @IBOutlet var subscribeButton: UIButton!
  @IBOutlet var activityIndicatorView: UIActivityIndicatorView!
  
  // MARK: - Instance Properties
  
  override var preferredStatusBarStyle: UIStatusBarStyle { return .lightContent }
  
  // MARK: - Object Lifecycle
  
  deinit {
    NotificationCenter.default.removeObserver(self)
  }
  
  // MARK: - View Lifecycle
  
  override func viewDidLoad() {
    super.viewDidLoad()
    NotificationCenter.default.addObserver(self,
                                           selector: #selector(handleSessionId(notification:)),
                                           name: SubscriptionService.sessionIdSetNotification,
                                           object: nil)
  }
  
  func handleSessionId(notification: Notification) {
    OperationQueue.main.addOperation { [weak self] in
      self?.configureView()
    }
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    navigationController?.setNavigationBarHidden(true, animated: true)
    configureView()
  }
  
  private func configureView() {
      subscribeButton.isHidden = false
      enterButton.isHidden = false
      activityIndicatorView.stopAnimating()
  }
}
