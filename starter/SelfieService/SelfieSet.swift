/**
 * Copyright (c) 2017 Razeware LLC
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

import Foundation

public struct SelfieSet {
  public let name: String
  public let selfies: [Selfie]
  
  init(name: String, selfies: [Selfie]) {
    self.name = name
    self.selfies = selfies
  }
  
  init?(name: String, nameAndFileMap: [String: String]) {
    let bundle = Bundle(for: SelfieService.self)
    let url = bundle.bundleURL.appendingPathComponent("Selfies", isDirectory: true)
    guard FileManager.default.fileExists(atPath: url.path) else {
      return nil
    }
    
    let selfies = nameAndFileMap.map { (name, fileName) -> Selfie in
      let imageUrl = url.appendingPathComponent(fileName)
      let imageData = try! Data(contentsOf: imageUrl)
      let image = UIImage(data: imageData)!
      let selfie = Selfie(name: name, image: image)
      return selfie
    }
    
    self.name = name
    self.selfies = selfies
  }
  
  func setLimitedToOneSelfie() -> SelfieSet {
    if let firstSelfie = selfies.first {
      return SelfieSet(name: name, selfies: [firstSelfie])
    } else {
      return self
    }
  }
}
