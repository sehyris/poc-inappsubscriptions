/**
 * Copyright (c) 2017 Razeware LLC
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

private let itcAccountSecret = "5f522d96905448359427722bb46b145e"

import Foundation

public enum Result<T> {
  case failure(SelfieServiceError)
  case success(T)
}

public typealias LoadSelfieCompletion = (_ selfies: Result<[SelfieSet]>) -> Void
public typealias UploadReceiptCompletion = (_ result: Result<(sessionId: String, currentSubscription: PaidSubscription?)>) -> Void

public typealias SessionId = String

public enum SelfieServiceError: Error {
  case missingAccountSecret
  case invalidSession
  case noActiveSubscription
  case other(Error)
}

public class SelfieService {
  
  static let mockSelfieData = [selfies1, selfies2, selfies3, selfies4, selfies5, selfies6]
  
  public static let shared = SelfieService()
//  let simulatedStartDate: Date
  
  private var sessions = [SessionId: Session]()
  
//  init() {
//    let persistedDateKey = "RWSSimulatedStartDate"
//    if let persistedDate = UserDefaults.standard.object(forKey: persistedDateKey) as? Date {
//      simulatedStartDate = persistedDate
//    } else {
//      let date = Date().addingTimeInterval(-30) // 30 second difference to account for server/client drift.
//      UserDefaults.standard.set(date, forKey: "RWSSimulatedStartDate")
//      
//      simulatedStartDate = date
//    }
//  }
  
  /// Trade receipt for session id
  public func upload(receipt data: Data, completion: @escaping UploadReceiptCompletion) {
    let body = [
      "receipt-data": data.base64EncodedString(),
      "password": itcAccountSecret
    ]
    let bodyData = try! JSONSerialization.data(withJSONObject: body, options: [])
    
    let url = URL(string: "https://sandbox.itunes.apple.com/verifyReceipt")!
    var request = URLRequest(url: url)
    request.httpMethod = "POST"
    request.httpBody = bodyData
    
    let task = URLSession.shared.dataTask(with: request) { (responseData, response, error) in
      if let error = error {
        completion(.failure(.other(error)))
      } else if let responseData = responseData {
        let json = try! JSONSerialization.jsonObject(with: responseData, options: []) as! Dictionary<String, Any>
        let session = Session(receiptData: data, parsedReceipt: json)
        self.sessions[session.id] = session
        let result = (sessionId: session.id, currentSubscription: session.currentSubscription)
        
        let date = Date()
        print("Date \(date)");
        UserDefaults.standard.set(date, forKey: "RWSSimulatedStartDate")
        
        completion(.success(result))
      }
    }
    
    task.resume()
  }
  
  /// Use sessionId to get selfies
  public func selfies(for sessionId: SessionId, completion: LoadSelfieCompletion?) {
    guard itcAccountSecret != "YOUR_ACCOUNT_SECRET" else {
      completion?(.failure(.missingAccountSecret))
      return
    }
    
    guard let _ = sessions[sessionId] else {
      completion?(.failure(.invalidSession))
      return
    }
    
    let paidSubscriptions = paidSubcriptions(for: sessionId)
    guard paidSubscriptions.count > 0 else {
      completion?(.failure(.noActiveSubscription))
      return
    }
    
    var selfieSets = [SelfieSet]()
    for (index, subscription) in paidSubscriptions.enumerated() {
      guard let set = selfieSet(number: index) else { continue }
      switch subscription.level {
      case .one:
        selfieSets.append(set.setLimitedToOneSelfie())
      case .all:
        selfieSets.append(set)
      }
    }
    
    completion?(.success(selfieSets))
  }
  
//  private func paidSubcriptions(since date: Date, for sessionId: SessionId) -> [PaidSubscription] {
//    if let session = sessions[sessionId] {
//      let subscriptions = session.paidSubscriptions.filter { $0.purchaseDate >= date }
//      return subscriptions.sorted { $0.purchaseDate < $1.purchaseDate }
//    } else {
//      return []
//    }
//  }
  
  private func paidSubcriptions(for sessionId: SessionId) -> [PaidSubscription] {
    if let session = sessions[sessionId] {
      let subscriptions = session.paidSubscriptions
      return subscriptions.sorted { $0.purchaseDate < $1.purchaseDate }
    } else {
      return []
    }
  }
  
  public func selfieSet(number setNumber: Int) -> SelfieSet? {
    guard setNumber < SelfieService.mockSelfieData.count else {
      return nil
    }
    
    let bundle = Bundle(for: type(of: self))
    let url = bundle.bundleURL.appendingPathComponent("Selfies", isDirectory: true)
    guard FileManager.default.fileExists(atPath: url.path) else {
      return nil
    }
    
    let selfieNames = SelfieService.mockSelfieData[setNumber]
    let selfies = selfieNames.map { (name, fileName) -> Selfie in
      let imageUrl = url.appendingPathComponent(fileName)
      let imageData = try! Data(contentsOf: imageUrl)
      let image = UIImage(data: imageData)!
      let selfie = Selfie(name: name, image: image)
      return selfie
    }
    
    return SelfieSet(name: "Set \(setNumber + 1)", selfies: selfies)
  }
  
}

private let selfies1 = [
  "Aaron Douglas":    "AaronDouglas-Stabby.jpg",
  "Adam Rush":        "AdamRush-NewYork.jpg",
  "Andy Obusek":      "AndyObusek.jpg"
]

private let selfies2 = [
  "Chris Wagner":     "ChrisWagner-RWPoster.jpg",
  "David Worsham":    "DavidWorsham-Tree.jpg",
  "Evan Dekhayser":   "EvanDekhayser-AirPods.jpg",
  "Greg Heo":         "GregHeo-Sunny.jpg",
  "Janie Clayton":    "JanieClayton-Peace.jpg",
  "Jessy and Catie":  "JessyAndCatie-Xmas.jpg",
  "Joshua and Ray":   "JoshAndRay.jpg"
]

private let selfies3 = [
  "Chris Wagner":     "ChrisWagner-SanDiego.jpg",
  "Fuad Kamal":       "FuadKamal-Filtered.jpg",
  "Janie Clayton":    "JanieClayton-Dinos.jpg",
  "Jessy and Catie":  "JessyAndCatie-Dog.jpg",
  "Kelvin Lau":       "KelvinLau-Panda.jpg",
  "Mike Gazdich":     "MikeGazdich-Bunny.jpg",
  "Richard Turton":   "RichardTurton-Book.jpg"
]

private let selfies4 = [
  "Jessy and Catie":    "JessyAndCatie-DogeOne.jpg",
  "Kelvin Lau":         "KelvinLau-Eyes.jpg",
  "Richard Turton":     "RichardTurton-Bourbon.jpg",
  "Tammy Coron":        "TammyCoron-TreeHat.jpg",
  "Tim Mitra":          "TimMitra-GregHeo.jpg",
  "Joshua and Family":  "JoshAndFamily.jpg"
]

private let selfies5 = [
  "Chris Language":   "ChrisLanguage-Driving.jpg",
  "Chris Wagner":     "ChrisWagner-Tesla.jpg",
  "Mike Gazdich":     "MikeGazdich-XmasSweater.jpg",
  "Richard Turton":   "RichardTurton-HelloKitty.jpg",
  "Tim Mitra":        "TimMitra-Scream.jpg"
]

private let selfies6 = [
  "Chris Wagner":     "ChrisWagner-Doge.jpg",
  "David Worsham":    "DavidWorsham-Tie.jpg",
  "Tim Mitra":        "TimMitra-CoolHouses.jpg",
  "Ray & Vicki":      "RayAndVicki.jpg",
  "Cesare Rocchi":    "CesareRocchi-Beer.jpg"
]
